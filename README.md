# Naamio AT Protocol Personal Data Service (PDS) Console

A command-line interface (CLI) to run and interace with the Naamio AT Protocol PDS. 

## Development

The project is structured to allow quick development and prototypign of features through the CLI. 

As components mature they can move from sub-crates to their own first-class projects.

## Formatting and linting

```shell
cargo fmt
cargo clippy -- -W clippy::pedantic
```